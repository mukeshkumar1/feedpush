var browserify = require('browserify');
var gulp = require('gulp');
var source = require("vinyl-source-stream");
var reactify = require('reactify'),
watch = require('gulp-watch');

gulp.task('browserify', function(){
  var b = browserify();
  b.transform(reactify); // use the reactify transform
  b.add('./public/javascripts/main.js');
  return b.bundle()
    .pipe(source('main.js'))
    .pipe(gulp.dest('./dist'));
});

gulp.task('default',["browserify"],function() {
	gulp.watch(['./public/javascripts/*.js','./public/javascripts/*.jsx'],["browserify"]);
});