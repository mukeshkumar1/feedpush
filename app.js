var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var agent = require('superagent');
var _ = require('underscore');
var async = require('async');
var fetchTags = require('./fetchTags');
var publish = require('./publish');
var config = require('./config');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use('/public',express.static(path.join(__dirname, 'dist')));

app.get('/',function(err,res) {
    res.render('index',{title:'Feed Publisher'});
});

app.get('/cortexlayout/:layout',function(req,res,next) {
    agent.get(config['cortex-endpoint']+req.params.layout,function(err,response){
        if(!err){
            return res.send(response.body);
        }
        next();
    });
});

app.get('/myxlayout/:platform/:layout',function(req,res,next) {
    agent.get(config['myx-endpoint']+req.params.platform+"/"+req.params.layout,function(err,response){
        if(!err){
            return res.send(response.body);
        }
        next();
    });
});

app.post('/fetch',function(req,resp,next) {
    fetchTags(req.body,function(err,res) {
        if(!err){
            resp.send(res);
        }else{
            resp.send(500);
        }
    })
});

app.post('/publish',function(req,resp,next) {
    var publishes = req.body;
    publish(publishes,function(err,res) {
        if(!err){
            resp.send(res);
        }else{
            resp.send(500);
        }
    });
});

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
