var agent = require('superagent');
var _ = require('underscore');
var async = require('async');
var config = require('./config');
var md5  = require('md5');

module.exports = function(publishes,cb) {
	var feedJson = require('./feed');
    var requests = _.map(publishes,function(banner) {
        var feedJson = JSON.parse(JSON.stringify(require('./feed')));
        feedJson.imageUrl = banner.src;

        feedJson.url = 'http://www.myntra.com'+banner.url;
        // feedJson.title = banner.url.indexOf("?") == -1 ? banner.url.substr(1).split("-").map(function(z){ return z.charAt(0).toUpperCase() + z.substr(1) }).join(" "):banner.url.substr(1,banner.url.indexOf("?")-1).split("-").map(function(z){ return z.charAt(0).toUpperCase() + z.substr(1) }).join(" ");
        
        _.each(banner.tags,function(tag) { feedJson.topics.push(tag) });
		feedJson.refId = md5(JSON.stringify(feedJson));
		console.log(JSON.stringify(feedJson));
        return function(callback){
            agent.post(config['qa-publish-endpoint'])
            .send(feedJson)
            .set('Authorization',config['authorization'])
            .set('Content-Type','application/json')
            .set('Accept','application/json')
            .end(callback);
        }
    });
    async.parallel(requests,function(err,res) {
            if(!err){
                return cb(null,res)
            }else{
            	console.log('err',err);
                cb(err);
            }

        })
};