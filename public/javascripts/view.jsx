var agent = require('superagent'),
	React = require('react'),
	_ = require('underscore');

var view = React.createClass({
	getInitialState:function() {
		return {loading:false ,fetching:false,fetched:false, banners:[],tags:[],layout:'cortex'};
	},
	getBanners:function() {
		var symlink = this.refs.symlink.getDOMNode().value;
		var that = this;
		if(symlink && this.state.layout === 'cortex'){
			agent.get('/cortexlayout/'+symlink,function(err,res){
				if(!err){
				var banners = getCortexBanners(res.body);
				 that.setState({loading:false , banners : banners});
			}
			else
				console.log(err) && that.setState({loading:false});
			});
			this.setState({loading:true});
		}
		else if(symlink && this.state.layout === 'myx'){
			agent.get('/myxlayout/'+symlink,function(err,res){
				if(!err){
				var banners = getMyxBanners(res.body);
				 that.setState({loading:false , banners : banners});
			}
			else
				console.log(err) && that.setState({loading:false});
			});
			this.setState({loading:true});
		}
		else{
			window.alert('Please enter symlink');
		}
			
	},
	setChecked : function(index,e) {
		var banners = this.state.banners;
		banners[index].set = e.target.checked;
		this.setState({banners:banners});
	},
	fetch:function() {
		var publishes = _.reject(this.state.banners,function(banner) {
			return !banner.set;
		});
		var that = this;
		if(publishes.length>0){
			this.setState({publishing:true});
			agent.post('/fetch')
			.send(publishes)
			.end(function(err,res) {
				if(!err){
					that.refs.symlink.getDOMNode().value ="";
					console.log(res.body);
					that.setState({fetching:false,banners:[] ,fetched:true,tags:res.body});
				}
				else{
					window.alert('Unable to publish');
				}
				
			});
		}
		else{
			window.alert('Please select banner to upload');
		}

	},
	publish:function() {
		var that = this;
		agent.post("/publish")
		.send(this.state.tags)
		.end(function(err,res){
			if(!err){
				window.alert('Published succesfully');
			}else{
				window.alert('Unable to publish');
			}
			that.setState(that.getInitialState());
		});
	},
	setBannerSelector:function(e) {
		var layouts = ["cortex","myx"];
		this.setState({layout:layouts[e.target.selectedIndex]});
	},
	renderLayoutSelector:function() {
		var cortexSelected = this.state.layout === 'cortex' ? true : false;
		return (<select value="layouts" onChange = {this.setBannerSelector} value = {cortexSelected?'cortex':'myx'}>
				    <option value="cortex" >cortex</option>
				    <option value="myx" >myx</option>
				</select>);
	},
	renderBannerSelector:function() {
		var banners = this.state.banners;
		if(banners.length > 0 && !this.state.fetching){
			var that = this
			var elements = _.map(banners,function(banner,index) {
				return (<div>
								<input type ="checkbox" value = {banner.href} ref = "selectLayouts" checked = {banner.set} onChange = {_.partial(that.setChecked,index)}/>
								<span> {banner.href} </span>
								</div>);

			});
			elements.unshift((<div> <input type = "button" value = "Get Banner Specs for publishing" onClick = {this.fetch}/> </div>))
			return elements;
		}
		else if(this.state.fetching){
			return <div><span>Fetching Details</span><img src = '/public/loading.gif' style = {{width:50}}/></div>
		}
		else{
			return "";
		}
	},
	renderTags:function() {
		var elements = _.map(this.state.tags,function(searchTags) {
			var tags = JSON.stringify(searchTags.tags);
			var rows = _.map(tags)
			return (
				  <tr style = {{padding:20}}>
				    <td>{searchTags.url}</td>
				    <td>{searchTags.src}</td>
				    <td>{tags}</td> 
				  </tr>);
		});
		return (<div>
				<table>
				  <tr>
				    <th>URL</th>
				    <th>Image</th>
				    <th>Tags</th>
				  </tr>
				  {elements}
				</table></div>);
	},
	render:function() {
		var loading = "";
		var banners = this.renderBannerSelector();
		var bannerSelector = this.renderLayoutSelector();
		var symlinkText = this.state.layout === 'cortex' ? "Enter cortex Symlink":"Enter platform/layout for myx";
		if(this.state.loading){
			loading = (<div><span>Loading</span><img src = '/public/loading.gif' style = {{width:50}}/></div>)
		}
		if(!this.state.fetched)
		{
			return (
		      <div>
		      	<div><span> Select layout type: </span>{bannerSelector}</div>
		      	<div>{symlinkText}</div>
		        <input type = "text" ref = "symlink"/>
		        <input type = "button" value = "Get Banners" onClick = {this.getBanners}/>
		        {loading}
		        {banners}       
		      </div>
		    );
		}
		else{
			var tags = this.renderTags();
		return(<div>{tags}<input type="button" value="Publish!" onClick = {this.publish}/></div>);
		}
	},
});

function getCortexBanners(cortexLayout){
	var banners = [];
	if(cortexLayout.type == 'image'){
		cortexLayout.props.href && cortexLayout.props.href.substr(0,31)!=='http://www.myntra.com/lookgood/' && cortexLayout.props.href.substr(0,6)!=='/shop/' ?banners.push({src:cortexLayout.props.src , href:cortexLayout.props.href,set:false}):null;
	}
	else{
		var childrenBanners = _.map(cortexLayout.children,function(layout) {
			return getCortexBanners(layout);
		});
		childrenBanners = _.flatten(childrenBanners);
		banners = _.union(banners,childrenBanners);
	}
	return banners;
}

function getMyxBanners(myxLayout){
	var banners = [];
	if(myxLayout.type == 'banner'){
		myxLayout.props.url && myxLayout.props.url.substr(0,31)!=='http://www.myntra.com/lookgood/' && myxLayout.props.url.substr(0,6)!=='/shop/' && myxLayout.props.url.substr(0,6)!=='/Shop/' ?banners.push({src:myxLayout.props.src , href:myxLayout.props.url,set:false}):null;
	}
	else{
		var childrenBanners = _.map(myxLayout.children,function(layout) {
			return getMyxBanners(layout);
		});
		childrenBanners = _.flatten(childrenBanners);
		banners = _.union(banners,childrenBanners);
	}
	return banners;
}

module.exports = view;