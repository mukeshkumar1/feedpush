var React = require('react');

var view = require('./view.jsx'); // need to specify the jsx extension
React.render(React.createElement(view,null), document.getElementById('content'));