module.exports = {
	'cortex-endpoint':'http://cortex-admin1.mynt.myntra.com/layout/',
	'myx-endpoint':'http://developer.myntra.com/layouts/',
	'qa-publish-endpoint' : 'http://qa_lg1:7099/myntra-pumps-service/lgp/pumps/object/publish/',
	'search-service-endpoint':'http://developer.myntra.com/search/data',
	'filters':["Department","Brand"],
	'filters-type':{"Department":"gender","Brand":"brand","Sections":"sections"},
	'authorization':'Basic U3lzdGVtflN5c3RlbTpTeXN0ZW0='
};