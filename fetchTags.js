var agent = require('superagent');
var _ = require('underscore');
var async = require('async');
var config = require('./config');

module.exports = function(banners,cb) {
    var requests = _.map(banners,function(banner) {
        return function(callback){
            agent.get(config['search-service-endpoint']+banner.href,function(err,res){
                res.href = banner.href;
                res.src = banner.src;
                callback(err,res);
            });
        };
    });
    async.parallel(requests,function(err,res){
        if(!err){
            var response = _.map(res,function(searchResponse){
                var url = searchResponse.href;
                var imageSrc = searchResponse.src;
                var tags = [];
                _.each(searchResponse.body.data.results.filters,function(filter){
                    if(_.contains(config['filters'],filter.title)){
                        var values = _.first(filter.values,5);
                        _.each(values,function(value){
                            tags.push({type:config['filters-type'][filter.title],name:value.option});
                        });
                    }
                });
                return {url:url,src:imageSrc,tags:tags};
            });
            cb(null,response)
        }
        else{
            cb(err);
        }
    })
};