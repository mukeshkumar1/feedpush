dev:
	NODE_ENV=development ./bin/www

setup:
	mkdir dist;
	npm install && gulp browserify && cp ./public/images/loading.gif ./dist

watch:
	gulp;

build:
	gulp browserify;